﻿Public Class form1
    Public p1 As Pen
    Public p2 As Pen
    Public p3 As Pen
    Public p4 As Pen
    Public p5 As Pen
    Public p6 As Pen
    Public p7 As Pen
    Public p8 As Pen
    Public p9 As Pen
    Private Sub NumericUpDown1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown1.ValueChanged
        If NumericUpDown1.Value = 361 Then
            NumericUpDown1.Value = 0
        ElseIf NumericUpDown1.Value = -1 Then
            NumericUpDown1.Value = 360
        End If
        PictureBox1.Invalidate()
    End Sub
    Public H As Integer
    Public n As Integer
    Private Sub PictureBox1_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PictureBox1.Paint
        H = NumericUpDown1.Value
        Dim g As Graphics = e.Graphics
        g.DrawLine(Pens.Black, 0, 225, 450, 225)
        g.DrawLine(Pens.Black, 225, 0, 225, 450)
        g.DrawArc(Pens.Black, 125, 125, 200, 200, 0, 360)
        g.DrawLine(Pens.Black, 0, 125, 450, 125)
        g.DrawLine(Pens.Black, 325, 0, 325, 450)
        n = 360 - NumericUpDown1.Value
        Dim m As Integer = 360 - NumericUpDown1.Value / 2
        Dim y As Integer = (Math.Sin(n * 2 * Math.PI / 360)) * 350
        Dim x As Integer = (Math.Cos(n * 2 * Math.PI / 360)) * 350
        g.DrawLine(p1, 225, 225, x + 225, y + 225)
        g.DrawLine(p1, 226, 226, x + 226, y + 226)
        g.DrawLine(p1, 225, 225, -1 * x + 225, -1 * y + 225)
        If n = 90 Or n = 270 Then
            Label4.Text = "تعریف نشده"
            Label5.Text = "0"
            Label3.Text = "0"

        ElseIf n = 180 Or n = 360 Or n = 0 Then
            Label5.Text = "تعریف نشده"
            Label4.Text = "0"
        Else
            Dim ta As Integer = (Math.Tan(n * 2 * Math.PI / 360)) * 100
            g.DrawLine(p4, 325, 225, 325, 225 + ta)
            g.DrawLine(p4, 326, 225, 326, 225 + ta)
            g.DrawLine(p4, 324, 225, 324, 225 + ta)
            g.DrawLine(p4, 327, 225, 327, 225 + ta)
            g.DrawLine(p4, 323, 225, 323, 225 + ta)
            Label4.Text = (Math.Tan(H * 2 * Math.PI / 360))
            Dim COT As Integer = 1 / (Math.Tan(n * 2 * Math.PI / 360)) * 100
            g.DrawLine(p5, 225, 125, 225 - COT, 125)
            g.DrawLine(p5, 225, 124, 225 - COT, 124)
            g.DrawLine(p5, 225, 126, 225 - COT, 126)
            g.DrawLine(p5, 225, 123, 225 - COT, 123)
            g.DrawLine(p5, 225, 127, 225 - COT, 127)
            Label4.Text = (Math.Tan(H * 2 * Math.PI / 360))
            Label5.Text = 1 / (Math.Tan(H * 2 * Math.PI / 360))
        End If
        Dim sin As Integer = (Math.Cos(n * 2 * Math.PI / 360)) * 100
        g.DrawLine(p3, 225, 225, 225 + sin, 225)
        g.DrawLine(p3, 225, 226, 225 + sin, 226)
        g.DrawLine(p3, 225, 224, 225 + sin, 224)
        g.DrawLine(p3, 225, 227, 225 + sin, 227)
        g.DrawLine(p3, 225, 223, 225 + sin, 223)
        Dim cos As Integer = (Math.Sin(n * 2 * Math.PI / 360)) * 100
        g.DrawLine(p2, 225, 225, 225, 225 + cos)
        g.DrawLine(p2, 224, 225, 224, 225 + cos)
        g.DrawLine(p2, 226, 225, 226, 225 + cos)
        g.DrawLine(p2, 223, 225, 223, 225 + cos)
        g.DrawLine(p2, 227, 225, 227, 225 + cos)
        Label2.Text = (Math.Sin(H * 2 * Math.PI / 360))
        y = (Math.Sin(m * 2 * Math.PI / 360)) * 25
        x = (Math.Cos(m * 2 * Math.PI / 360)) * 25
        g.DrawString("α", Me.Font, Brushes.Black, x + 225, y + 225)
        g.DrawString("SIN", Me.Font, Brushes.Black, 200, 10)
        g.DrawString("COS", Me.Font, Brushes.Black, 400, 210)
        g.DrawString("TAN", Me.Font, Brushes.Black, 300, 10)
        g.DrawString("COT", Me.Font, Brushes.Black, 400, 110)
        g.DrawLine(Pens.Black, 400, 125, 390, 115)
        g.DrawLine(Pens.Black, 399, 125, 390, 114)
        g.DrawLine(Pens.Black, 400, 125, 390, 135)
        g.DrawLine(Pens.Black, 399, 125, 390, 134)
        g.DrawLine(Pens.Black, 400, 225, 390, 215)
        g.DrawLine(Pens.Black, 399, 225, 390, 214)
        g.DrawLine(Pens.Black, 400, 225, 390, 235)
        g.DrawLine(Pens.Black, 399, 225, 390, 234)
        g.DrawLine(Pens.Black, 225, 50, 235, 60)
        g.DrawLine(Pens.Black, 225, 51, 235, 61)
        g.DrawLine(Pens.Black, 225, 50, 215, 60)
        g.DrawLine(Pens.Black, 225, 51, 215, 61)
        g.DrawLine(Pens.Black, 325, 50, 335, 60)
        g.DrawLine(Pens.Black, 325, 51, 335, 61)
        g.DrawLine(Pens.Black, 325, 50, 315, 60)
        g.DrawLine(Pens.Black, 325, 51, 315, 61)
        Label3.Text = (Math.Cos(H * 2 * Math.PI / 360))
        g.DrawString("O", Me.Font, Brushes.Black, 212, 230)
        g.DrawString("A", Me.Font, Brushes.Black, 312, 230)
        g.DrawString("A'", Me.Font, Brushes.Black, 112, 230)
        g.DrawString("B'", Me.Font, Brushes.Black, 212, 330)
        g.DrawString("B", Me.Font, Brushes.Black, 212, 130)
        g.DrawArc(Pens.Black, 175, 175, 100, 100, n, H)
        If n = 90 Then
            Label2.Text = "-1"
            Label3.Text = "0"
        ElseIf n = 270 Then
            Label2.Text = "1"
            Label3.Text = "0"
        End If
        If n = 0 Or n = 360 Then
            Label2.Text = "0"
            Label3.Text = "1"
        ElseIf n = 180 Then
            Label2.Text = "0"
            Label3.Text = "-1"
        End If
        If CheckBox1.Checked = True Then
            Dim y2 As Integer = (Math.Sin(-n * 2 * Math.PI / 360)) * 350
            Dim x2 As Integer = (Math.Cos(-n * 2 * Math.PI / 360)) * 350
            g.DrawArc(p6, 200, 200, 50, 50, 0, H)
            g.DrawLine(p6, 225, 225, x2 + 225, y2 + 225)
            Label10.Text = (Math.Cos(-H * 2 * Math.PI / 360))
            Label8.Text = (Math.Sin(-H * 2 * Math.PI / 360))
            Label11.Text = (Math.Tan(-H * 2 * Math.PI / 360))
            Label12.Text = 1 / (Math.Tan(H * 2 * Math.PI / 360))
            Label17.Text = "α'=" & -H
            If -H = 0 Or -H = -360 Then
                Label8.Text = 0
                Label10.Text = 1
                Label11.Text = 0
                Label12.Text = "بینهایت"
            ElseIf -H = 90 Or -H = -270 Then
                Label8.Text = 1
                Label10.Text = 0
                Label11.Text = "بینهایت"
                Label12.Text = 0
            ElseIf -H = 180 Or -H = -180 Then
                Label8.Text = 0
                Label10.Text = -1
                Label11.Text = 0
                Label12.Text = "بینهایت"
            ElseIf -H = 270 Or -H = -90 Then
                Label8.Text = -1
                Label10.Text = 0
                Label11.Text = "بینهایت"
                Label12.Text = 0
            End If
        End If
        If CheckBox2.Checked = True Then

            Dim y3 As Integer = (Math.Sin(((360 - 180) - (360 - H)) * 2 * Math.PI / 360)) * 350
            Dim x3 As Integer = (Math.Cos(((360 - 180) - (360 - H)) * 2 * Math.PI / 360)) * 350
            g.DrawLine(p7, 225, 225, x3 + 225, y3 + 225)
            g.DrawArc(p7, 205, 205, 40, 40, 0, ((360 - 180) - (360 - H)))
            Label22.Text = (Math.Cos(-((360 - 180) - (360 - H)) * 2 * Math.PI / 360))
            Label21.Text = (Math.Sin(-((360 - 180) - (360 - H)) * 2 * Math.PI / 360))
            Label23.Text = (Math.Tan(-((360 - 180) - (360 - H)) * 2 * Math.PI / 360))
            Label24.Text = 1 / (Math.Tan(-((360 - 180) - (360 - H)) * 2 * Math.PI / 360))
            Label18.Text = "α''=" & -((360 - 180) - (360 - H))
            Dim j As Integer = -((360 - 180) - (360 - H))
            If j = 0 Or j = 360 Then
                Label21.Text = 0
                Label22.Text = 1
                Label23.Text = 0
                Label24.Text = "بینهایت"
            ElseIf j = 90 Then
                Label21.Text = 1
                Label22.Text = 0
                Label23.Text = "بینهایت"
                Label24.Text = 0
            ElseIf j = 180 Or j = -180 Then
                Label21.Text = 0
                Label22.Text = -1
                Label23.Text = 0
                Label24.Text = "بینهایت"
             ElseIf j = 270 Or j = -90 Then
                Label21.Text = -1
                Label22.Text = 0
                Label23.Text = "بینهایت"
                Label24.Text = 0
            End If
        End If
        If CheckBox3.Checked = True Then
            Dim y4 As Integer = (Math.Sin(((360 - 90) - (360 - H)) * 2 * Math.PI / 360)) * 350
            Dim x4 As Integer = (Math.Cos(((360 - 90) - (360 - H)) * 2 * Math.PI / 360)) * 350
            g.DrawArc(p8, 195, 195, 60, 60, 0, ((360 - 90) - (360 - H)))
            g.DrawLine(p8, 225, 225, x4 + 225, y4 + 225)
            Label26.Text = (Math.Cos(-((360 - 90) - (360 - H)) * 2 * Math.PI / 360))
            Label25.Text = (Math.Sin(-((360 - 90) - (360 - H)) * 2 * Math.PI / 360))
            Label27.Text = (Math.Tan(-((360 - 90) - (360 - H)) * 2 * Math.PI / 360))
            Label28.Text = 1 / (Math.Tan(-((360 - 90) - (360 - H)) * 2 * Math.PI / 360))
            Label19.Text = "α''=" & -((360 - 90) - (360 - H))
            Dim j As Integer = -((360 - 90) - (360 - H))
            If j = 0 Or j = 360 Then
                Label25.Text = 0
                Label26.Text = 1
                Label27.Text = 0
                Label28.Text = "بینهایت"
            ElseIf j = 90 Or j = -270 Then
                Label25.Text = 1
                Label26.Text = 0
                Label27.Text = "بینهایت"
                Label28.Text = 0
            ElseIf j = 180 Or j = -180 Then
                Label25.Text = 0
                Label26.Text = -1
                Label27.Text = 0
                Label28.Text = "بینهایت"
            ElseIf j = 270 Or j = -90 Then
                Label25.Text = -1
                Label26.Text = 0
                Label27.Text = "بینهایت"
                Label28.Text = 0
            End If
        End If
        If CheckBox4.Checked = True Then
            Panel1.BackColor = Color.Black
        Else
            Panel1.BackColor = Color.White
        End If

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        p1 = Pens.Black
        p2 = Pens.Red
        p3 = Pens.Lime
        p4 = Pens.Blue
        p5 = Pens.Yellow
        p6 = New Pen(Color.FromArgb(255, 255, 128, 128))
        p7 = New Pen(Color.FromArgb(255, 255, 128, 0))
        p8 = New Pen(Color.FromArgb(255, 128, 255, 128))
        p9 = Pens.Red
    End Sub
    Public c As ColorDialog = New ColorDialog
    Public D As DialogResult
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        D = (c.ShowDialog())
        If D = Windows.Forms.DialogResult.Cancel Then
            Return
        End If
        p1 = New Pen(c.Color)
        Button1.BackColor = p1.Color
        Label1.ForeColor = p1.Color
        PictureBox1.Invalidate()
    End Sub
    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Label8.Enabled = True
            Label10.Enabled = True
            Label11.Enabled = True
            Label12.Enabled = True
            Label17.Enabled = True
        Else
            Label17.Enabled = False
            Label8.Enabled = False
            Label10.Enabled = False
            Label11.Enabled = False
            Label12.Enabled = False
            Label8.Text = "0"
            Label10.Text = "0"
            Label11.Text = "0"
            Label12.Text = "0"
            Label17.Text = "α="
        End If
        PictureBox1.Invalidate()

    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then
            Label21.Enabled = True
            Label22.Enabled = True
            Label23.Enabled = True
            Label24.Enabled = True
            Label18.Enabled = True
        Else
            Label18.Enabled = False
            Label21.Enabled = False
            Label22.Enabled = False
            Label23.Enabled = False
            Label24.Enabled = False
            Label21.Text = "0"
            Label22.Text = "0"
            Label23.Text = "0"
            Label24.Text = "0"
            Label18.Text = "α="
        End If
        PictureBox1.Invalidate()
    End Sub

    Private Sub Label18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label18.Click

    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click

    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            Label25.Enabled = True
            Label26.Enabled = True
            Label27.Enabled = True
            Label28.Enabled = True
            Label19.Enabled = True
        Else
            Label19.Enabled = False
            Label25.Enabled = False
            Label26.Enabled = False
            Label27.Enabled = False
            Label28.Enabled = False
            Label25.Text = "0"
            Label26.Text = "0"
            Label27.Text = "0"
            Label28.Text = "0"
            Label19.Text = "α'="
        End If
        PictureBox1.Invalidate()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        D = c.ShowDialog()
        If D = Windows.Forms.DialogResult.Cancel Then
            Return
        End If
        p6 = New Pen(c.Color)
        Button6.ForeColor = p6.Color
        Label8.ForeColor = p6.Color
        Label10.ForeColor = p6.Color
        Label11.ForeColor = p6.Color
        Label12.ForeColor = p6.Color
        Label17.ForeColor = p6.Color
        Label20.ForeColor = p6.Color
        CheckBox1.ForeColor = p6.Color
        PictureBox1.Invalidate()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        D = c.ShowDialog()
        If D = Windows.Forms.DialogResult.Cancel Then
            Return
        End If
        p8 = New Pen(c.Color)
        Label19.ForeColor = p8.Color
        Label25.ForeColor = p8.Color
        Label26.ForeColor = p8.Color
        Label27.ForeColor = p8.Color
        Label28.ForeColor = p8.Color
        Button7.ForeColor = p8.Color
        Label30.ForeColor = p8.Color
        CheckBox3.ForeColor = p8.Color
        PictureBox1.Invalidate()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        D = c.ShowDialog()
        If D = Windows.Forms.DialogResult.Cancel Then
            Return
        End If
        p7 = New Pen(c.Color)
        Label18.ForeColor = p7.Color
        Label21.ForeColor = p7.Color
        Label22.ForeColor = p7.Color
        Label23.ForeColor = p7.Color
        Label24.ForeColor = p7.Color
        Button8.ForeColor = p7.Color
        Label29.ForeColor = p7.Color
        CheckBox2.ForeColor = p7.Color
        PictureBox1.Invalidate()
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        PictureBox1.Invalidate()
    End Sub
    Private Sub Button5_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        D = (c.ShowDialog())
        If D = Windows.Forms.DialogResult.Cancel Then
            Return
        End If
        p5 = New Pen(c.Color)
        Button5.BackColor = p5.Color
        Label37.ForeColor = p5.Color
        Label5.ForeColor = p5.Color
        PictureBox1.Invalidate()
    End Sub
    Private Sub Button4_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        D = (c.ShowDialog())
        If D = Windows.Forms.DialogResult.Cancel Then
            Return
        End If
        p4 = New Pen(c.Color)
        Button4.BackColor = p4.Color
        Label36.ForeColor = p4.Color
        Label4.ForeColor = p4.Color
        PictureBox1.Invalidate()
    End Sub
    Private Sub Button3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        D = (c.ShowDialog())
        If D = Windows.Forms.DialogResult.Cancel Then
            Return
        End If
        p3 = New Pen(c.Color)
        Button3.BackColor = p3.Color
        Label35.ForeColor = p4.Color
        Label3.ForeColor = p3.Color
        PictureBox1.Invalidate()
    End Sub
    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        D = (c.ShowDialog())
        If D = Windows.Forms.DialogResult.Cancel Then
            Return
        End If
        p2 = New Pen(c.Color)
        Button2.BackColor = p2.Color
        Label34.ForeColor = p4.Color
        Label2.ForeColor = p2.Color
        PictureBox1.Invalidate()
    End Sub
End Class
